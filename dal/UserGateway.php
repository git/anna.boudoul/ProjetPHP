<?php

// password_hash
// password_verify
    class UserGateway{
        private $con;

        public function __construct(Connection $con) { 
        $this->con = $con; }

        public function create($login, $mdp){
            $pwrd = password_hash($mdp, PASSWORD_BCRYPT, array("cost" => 12));
            $query = 'INSERT INTO User VALUES (:login, :mdp)';
            $this->con->executeQuery($query,  array(':login'=>array($login, PDO::PARAM_STR),
                                            ':mdp'=>array($pwrd, PDO::PARAM_STR)));
            $result = $this->con->getResults();
            return $result;
        }

        public function updateLogin($oldValue, $newValue){
            $query = 'UPDATE User SET login = :newValue WHERE login = :oldValue';
            $this->con->executeQuery($query,  array(':oldValue'=>array($oldValue, PDO::PARAM_STR),
                                                    ':newValue'=>array($newValue, PDO::PARAM_STR)));
        }

        public function updateMdp($login, $mdp){
            $pwrd = password_hash($mdp, PASSWORD_BCRYPT, array("cost" => 12));
            $query = 'UPDATE User SET mdp = :value WHERE login = :login';
            $this->con->executeQuery($query,  array(':login'=>array($login, PDO::PARAM_STR),
                                                ':value'=>array($pwrd, PDO::PARAM_STR)));
        }

        public function delete($value){
            $query = 'DELETE FROM User WHERE login = :value';
            $this->con->executeQuery($query,  array(':value'=>array($value, PDO::PARAM_STR)));
        }
        
        public function find($value, $parameterkind){
            $query = 'SELECT login FROM User WHERE :parameterkind = :value';
            $this->con->executeQuery($query,  array(':value'=>array($value, PDO::PARAM_STR),
                                            ':parameterkind'=>array($parameterkind, PDO::PARAM_STR)));
            $result = $this->con->getResults();
            return $result;
        }

        public function findUser($login, $mdp){
            $query = 'SELECT mdp FROM User WHERE login = :login';
            $this->con->executeQuery($query,  array(':login'=>array($login, PDO::PARAM_STR)));
            $result = $this->con->getResults();
            if(empty($result[0][0])) return;
            if(password_verify($mdp, $result[0][0]))
                return $login;
        }
    }
?>