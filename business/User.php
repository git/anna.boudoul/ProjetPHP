<?php
    class User{
        private string $login; // botar um id no final se der tempo

        function __construct($login) {
            $this->login = $login;
        }

        function get_login() {
            return $this->login;
        }

        function set_login($login) {
            $this->login = $login;
        }
    }
?>