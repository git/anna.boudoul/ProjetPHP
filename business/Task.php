<?php
    class Task{
        private int $id;
        private string $titre;
        private string $description;
        private $dateDeb;
        private $dateFin; # $today = date("m.d.y")
        private string $priorite;

	    private string $idlist; // # id associating task to list
        private bool $isDone; // # si la tache est complete
        
	function __construct($titre,$description,$priorite,$idl,$dateDeb=null,$dateFin=null,$isDone=false,$id=0) {
            # if id = 0, on veut donc creer une nouvelle tache
            # qui n'est pas encore dans la base de donnée et donc
            # le id sera fait avec le auto increment
            if($id==0)
                $this->set_id((int)null);
            else
                $this->set_id($id);
            $this->set_titre($titre);
            $this->set_description($description);
            $this->set_priorite($priorite);
            $this->set_dateDeb($dateDeb);
            $this->set_dateFin($dateFin);
            $this->set_idlist($idl);
            $this->set_isDone($isDone);
        }

        function get_id() {
            return $this->id;
        }

        function set_id($id) {
            $this->id = $id;
        }

        function get_titre() {
            return $this->titre;
        }

        function set_titre($titre) {
            $this->titre = $titre;
        }

        function get_description() {
            return $this->description;
        }

        function set_description($description) {
            $this->description = $description;
        }

        function get_dateDeb() {
            return $this->dateDeb;
        }

        function set_dateDeb($dateDeb) {
            $this->dateDeb = $dateDeb;
        }

        function get_dateFin() {
            return $this->dateFin;
        }

        function set_dateFin($dateFin) {
            $this->dateFin = $dateFin;
        }

        function get_priorite() {
            return $this->priorite;
        }

        function set_priorite($priorite) {
            $this->priorite = $priorite;
	    }
        // # getter et setter pour idList
        function get_idList() {
                return $this->idList;
        }
        
        function set_idList($idList) {
                $this->idList = $idList;
        }

        function get_isDone() {
                return $this->isDone;
        }
        
        function set_isDone($isDone) {
                $this->isDone = $isDone;
        }
     
    }
?>
