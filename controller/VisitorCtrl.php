<?php
require("model/TaskModel.php");

class VisitorCtrl
{
    private $taskModel;
    private $userModel;
    private $isUser;
    private $TabVues;

    function __construct($con, $TabVues, $isUser){
        $this->TabVues = $TabVues;
        $this->isUser = $isUser;
        $dvueErreur = array();
        $this->taskModel = new TaskModel($con);
        $this->userModel = new UserModel($con);
        try{
            if(isset($_REQUEST['action']))
                $action = $_REQUEST['action'];
            else
                $action = null;
            
            switch($action){
                case null:
                    $this->loadHome();
                    # initialize la page home avec tout les listes
                    break;

                case 'pageConnection':
                    $this->go_connection();
                    break;

                case 'pageAbout':
                    $this->go_about();
                    break;

                case 'connecter':
                    $this->connection();
                    # charge la vue de connexion
                    break;
                
                case 'pageRegister':
                    $this->go_register();
                    break;

                case 'register':
                    $this->register();
                    # charge la vue de connexion
                    break;

                case 'pageListe':
                    $this->go_list();
                    break;
                
                case 'creerListe':
                    $this->makeList();
                    # creer une liste publique
                    break;
                
                case 'supprimerListe':
                    $this->eraseList();
                    # supprime une liste publique
                    break;
                
                case 'pageTache':
                    $this->go_task();
                    break;
                
                case 'ajouterTache': 
                    $this->addTask();
                    # ajouter une tache a une liste pub
                    break;
                
                case 'supprimerTache':
                    $this->eraseTask();
                    # supprimer une tache
                    break;
                
                case 'isDone':
                    $this->isDone();
                    break;
                
                default:
                    $TMessage[] = 'Unexpected error';
                    require($this->TabVues["erreur"]); 
                    $this->loadHome();
                    break;
            }
            
        } catch (Exception $e) {
            var_dump($_POST);
            $TMessage[] = $e->getMessage();
            require($this->TabVues["erreur"]); 
        }
    }
    function loadHome(){
        $public_lists = $this->taskModel->loadPublicLists();
        # le if suivant est nécéssaire dans le cas ou l'action
        # connection a été appeller. Dans ce cas, loadHome doit
        # prendre en compte le user qui vient d'être ajouter a 
        # $_SESSION['login']
        if(isset($_SESSION['login']) && $_SESSION['login'] != "")
            $user = $_SESSION['login'];
        else
            $user = $this->isUser;

        require($this->TabVues["home"]); 
    }

    function go_connection(){
        $user = $this->isUser;
        require($this->TabVues["connection"]);
    }

    function go_about(){
        $user = $this->isUser;
        require($this->TabVues["about"]);
    }

    function connection(){
        $isCo = $this->userModel->connexion($_POST['username'],$_POST['password']);
        if(!$isCo){
            $TMessage[] = 'This user does not exist'; require($this->TabVues["erreur"]);
            $this->loadHome();
        }
        else
            $this->loadHome();
    }

    function go_register(){
        $user = $this->isUser;
        require($this->TabVues["register"]);
    }

    function register(){
        $this->userModel->ajouter($_POST['username'],$_POST['password']);
        $this->go_connection();
    }
    
    function go_list(){
        $user = $this->isUser;
        require("view/newList.php");
    }

    function makeList(){     
        $this->taskModel->addList($_POST['listName']);
        $this->loadHome();
    }

    function eraseList(){
        $this->taskModel->supList($_POST['listId']);
        $this->loadHome();
    }

    function go_task(){
        $user = $this->isUser;
        if(isset($_POST['listId'])) $_SESSION['listId'] = $_POST['listId'];
        if(isset($_POST['isPriv'])) $_SESSION['isPriv'] = $_POST['isPriv'];
        require("view/newTask.php");
    }

    function addTask(){
        $this->taskModel->addTask($_POST['titreT'],$_POST['descT'],
        $_POST['prioriteT'],$_SESSION['listId'],$_POST['dateDebT'],$_POST['dateFinT']);

        if(isset($_SESSION['isPriv']) && $_SESSION['isPriv'])
            header("Location:index.php?action=voirListePriv");
        else
            $this->loadHome();
    }

    function eraseTask(){
        $this->taskModel->supTask($_POST['idT']);
        if(!empty($_POST['isPriv']))
            header("Location:index.php?action=voirListePriv");
        else
            $this->loadHome();
    }
    
    function isDone(){
        if ($_POST['Tdone'])
            $this->taskModel->modifTask($_POST['idT'],'isDone',false);
        else
            $this->taskModel->modifTask($_POST['idT'],'isDone',true);
        if(!empty($_POST['isPriv']))
            header("Location:index.php?action=voirListePriv");
        else
            $this->loadHome();
    }
}
?>