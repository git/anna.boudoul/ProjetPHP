<?php
require_once("business/ListTask.php");
include_once("business/Task.php");


class TaskGateway
{
		// connection attribute 
		public Connection $con;
		
		// constructor
		public function __construct(Connection $con){
			$this->con=$con;
		}

		// functions
		// code de retour pour les fonctions i,u,d?
		public function insertT(Task $t){
			$query='INSERT INTO Tache VALUES (:id,:titre,:descript,:dateDeb,:dateFin,:priorite,:idList,false)';
            if($t->get_dateDeb() == null)
                $dateDeb = NULL;
            else
                $dateDeb = $t->get_dateDeb();
            if($t->get_dateFin() == null)
                $dateFin = NULL;
            else
                $dateFin = $t->get_dateFin();
            
            $this->con->executeQuery($query, array(
            ':id'=> array($t->get_id(),PDO::PARAM_STR),
            ':titre'=> array($t->get_titre(),PDO::PARAM_STR),
            ':descript'=> array($t->get_description(),PDO::PARAM_STR),
            ':dateDeb'=> array($dateDeb,PDO::PARAM_STR),
            ':dateFin'=> array($dateFin,PDO::PARAM_STR),
            ':priorite'=> array($t->get_priorite(),PDO::PARAM_STR),
            ':idList'=> array($t->get_idList(),PDO::PARAM_STR) ));
		}

		public function update($table,$id,$element, $valeur){
            if($table == 'task'){
                $query='UPDATE Tache SET '.$element.'=:'.$element.' WHERE id=:id';
            } else {
                $query='UPDATE uList SET '.$element.'=:'.$element.' WHERE id=:id';
            }
            $this->con->executeQuery($query, array(
            ':id'=>array($id,PDO::PARAM_STR),
            ':'.$element =>array($valeur,PDO::PARAM_STR)));
		}

		public function delete($table,$id){
            if($table == 'task'){
                $query='DELETE FROM Tache WHERE id = :id';
            } else {
                $query='DELETE FROM uList WHERE id = :id;
                        DELETE FROM Tache WHERE idList = :id;';
            }
            $this->con->executeQuery($query, array(
            ':id'=>array($id,PDO::PARAM_STR)));
		}

		/*si on veut trouver une liste, juste chercher toutes les taches avec idList= id_de_la_liste*/
        # pour toutes les listes d'un user specifique, appeller 2 fois la fonction:
            # une fois pour prendre toutes les listes qui ont l'id de l'user
            # pour toutes ces listes (foreach), find toutes chaque tache associé
        public function find($table, $element="", $valeur=""){
            $tabResult = array();
            if($table =='task'){
                if(strcmp($element,"")==0)
                {
                    $query='SELECT * FROM Tache';
                    $this->con->executeQuery($query);
                }
                else
                {
                    $query='SELECT * FROM Tache WHERE '.$element.'=:'.$element;
                    $this->con->executeQuery($query, array(
                    ':'.$element =>array($valeur,PDO::PARAM_STR)));
                }
                
                $results=$this->con->getResults();
                foreach($results as $row)
                {
                    $tabTaches[]=new Task($row['titre'],$row['description'],$row['priorite'],
                    $row['idList'],$row['dateDebut'],$row['dateFin'],$row['isDone'],$row['id']);    
                }
                return $tabResult;

		    } else if($table == 'list'){
                if(strcmp($element,"")==0)
                {
                    $query='SELECT * FROM uList';
                    $this->con->executeQuery($query);
                }
                else
                {
                    $query='SELECT * FROM uList WHERE '.$element.'=:'.$element;
                    $this->con->executeQuery($query, array(
                    ':'.$element =>array($valeur,PDO::PARAM_STR)));
                }
                
                $results=$this->con->getResults();
                foreach($results as $row)
                {
                    $tabList[]=new ListTask($row['nom'],$row['user'],$row['dc'],$row['id']);    
                }
                return $tabResult;
		    }
        }

// SELECT tache.id FROM Tache tache, Liste liste
// tache.idListe = liste.id AND liste.user IS NULL;

    /* # LIST FUNCTIONS  */
    /*create, update, delete, read(select info)*/
	    public function insertL(ListTask $l){
			$query='INSERT INTO uList VALUES (:id,:nom,:user,0)';
			$this->con->executeQuery($query, array(
            ':id'=> array($l->get_id(),PDO::PARAM_STR),
            ':nom'=> array($l->get_nom(),PDO::PARAM_STR),
            ':user'=> array($l->get_owner(),PDO::PARAM_STR)));
        }

        public function count($list){
            $query='SELECT COUNT(*) from Tache where idList = :idList';

            $this->con->executeQuery($query, array(
                ':idList' => array($list->get_id(),PDO::PARAM_INT)
            ));

            $result = $this->con->getResults();
            foreach($result as $row){
                $taille = $row[0];
            }
            return $taille;
        }

        public function findUserList($user){
            # pas réussit a faire une jointure optimale donc
            # decomposé en plusieurs foncitons:
            # findUserList
            # findTacheList

            $query='SELECT * from uList where user = :user';
            $this->con->executeQuery($query, array(
                ':user' => array($user,PDO::PARAM_STR)
            ));
            $tabLists = array();
            $results = $this->con->getResults();
            foreach($results as $row){
                $tabLists[]= new ListTask($row[1],$row[2],$row[3],$row[0]);
            }
            return $tabLists;
        }

        public function findListTask($list){
            $query='SELECT * from Tache where idList = :idList';

            $this->con->executeQuery($query, array(
                ':idList' => array($list->get_id(),PDO::PARAM_STR)
            ));

            $results = $this->con->getResults();
            
            foreach($results as $row){
                $taches[]= new Task($row['titre'],$row['description'],$row['priorite'],
                $row['idList'],$row['dateDebut'],$row['dateFin'],$row['isDone'],$row['id']);
            }
            if(!empty($taches))
                $list->set_taches($taches);
            return $list;
        }

        public function findPublicList(){
            $query='SELECT * from uList where user is NULL';
            $this->con->executeQuery($query);

            $tabLists = array();
            $results = $this->con->getResults();
            foreach($results as $row){
                $tabLists[]= new ListTask($row[1],$row[2],$row[3],$row[0]);
            }
            return $tabLists;
        }
	}
?>
