<?php
require_once("model/TaskModel.php");

class UserCtrl
{
    private $view;
    private $con;
    private $taskModel;
    private $userModel;
    private $TabVues;

    public function __construct(Connection $con, $TabVues){
        $this->TabVues = $TabVues;
        $this->con = $con;
        $this->userModel = new UserModel($this->con);
        $this->taskModel = new TaskModel($this->con);
        try{
            $action=$_REQUEST['action'];
            switch($action){

                // voir les listes privees 
                case 'voirListePriv': 
                    $this->loadListePriv();
                    break;
                
                // ajouter une liste privee
                case 'creerListePriv': 
                    $this->newListPrivate();
                    break;

                // supprimer une liste privee
                case 'supprimerListePriv': 
                    $this->erasePrivList();
                    break;

                case 'deconnecter': 
                    $this->deconnexion();
                    break;
                default:
                $TMessage[] = 'Unexpected error';
                require($this->TabVues["erreur"]); 
                break;
    
            }
        } catch(Exception $e) {
            $TMessage[] = $e->getMessage();
            require($this->TabVues["erreur"]); 
        }
    }

    public function loadListePriv(){
        $user = $_SESSION['login'];
        $private_lists = $this->taskModel->loadPrivateLists($user);
        require($this->TabVues["prives"]);  
    }

    public function newListPrivate(){
        $this->taskModel->addList($_POST['listName'],$_SESSION['login']);
        $this->loadListePriv();
    }

    function erasePrivList(){
        $this->taskModel->supList($_POST['listId']);
        $this->loadListePriv();
    }

    function loadHome(){
        $public_lists = $this->taskModel->loadPublicLists();
        require($this->TabVues["home"]); 
    }

    public function deconnexion(){
        $this->userModel->deconnexion();
        header("Location:index.php");
    }
}
?>
