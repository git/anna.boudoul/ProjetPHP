<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="view/css/home.css" rel="stylesheet" />
    </head>
    <body>
        <div class="d-flex" id="wrapper">
            <!-- Sidebar-->
            <div class="border-end bg-white" id="sidebar-wrapper">
                <div class="sidebar-heading border-bottom bg-light">2do</div>
                <div class="list-group list-group-flush">
                <a class="list-group-item list-group-item-action list-group-item-light p-3" href="index.php">Home</a>
                <a class="list-group-item list-group-item-action list-group-item-light p-3" href="index.php?action=pageListe">New List +</a>
                <?php
                    if($user){
                        echo '<a class="list-group-item list-group-item-action list-group-item-light p-3" href="index.php?action=voirListePriv">My Lists &#128274;</a>';
                    }               
                ?>
                <a class="list-group-item list-group-item-action list-group-item-light p-3" href="index.php?action=pageAbout">About</a>
                </div>
            </div>
            <!-- Page content wrapper-->
            <div id="page-content-wrapper">
                <!-- Top navigation-->
                <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                    <div class="container-fluid">
                        <button class="btn btn-primary" id="sidebarToggle">Toggle Menu</button>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ms-auto mt-2 mt-lg-0">
                                <?php
                                    if($user){
                                        echo '<li class="nav-item"><a class="nav-link" href="index.php?action=deconnecter">Log out</a></li>';
                                    } else {
                                        echo '<li class="nav-item"><a class="nav-link" href="index.php?action=pageConnection">Log In</a></li>';
                                        echo '<li class="nav-item"><a class="nav-link" href="index.php?action=pageRegister">Register</a></li>';
                                    }
                                ?>
                            </ul>
                        </div>
                    </div>
                </nav>
                <!--Contenue de la page ici-->
                <div class="container-fluid">
                <h4 class="text-center my-3 pb-3">New Task</h4>
                <form method="post" action="index.php">
                        <div class="form-outline mb-4 align-items-center">
                            <label class="form-label" for="form2title">Name</label>
                            <input name="titreT" type="text"  id="form2title" class="form-control" required minlength="0" maxlength="250"/>
                        </div>

                        <div class="form-outline mb-4">
                            <label class="form-label" for="form2description">Description</label>
                            <input name="descT" type="text" id="form2description" class="form-control" minlength="0" maxlength="250">
                        </div>

                        <div class="form-outline mb-4">
                            <label class="form-label" for="form2dateDeb">Starting date</label>
                            <input name="dateDebT" type="date" id="form2dateDeb" class="form-control">
                        </div>

                        <div class="form-outline mb-4">
                            <label class="form-label" for="form2dateFin">Ending date</label>
                            <input name="dateFinT" type="date" id="form2dateFin" class="form-control">
                        </div>

                        <!-- <div class="form-outline mb-4">
                            <input name="prioriteT" type="text" id="form2importance" class="form-control" />
                            <label class="form-label" for="form2importance">Importance</label>
                        </div> -->

                        <div class="form-outline mb-4">
                            <label class="form-label" for="form2importance">Importance</label>
                            <select name="prioriteT" id="form2importance" class="form-control">
                                <option value="">--Please choose an option--</option>
                                <option value="Urgent">Urgent</option>
                                <option value="Important">Important</option>
                                <option value="Medium">Medium</option>
                                <option value="Low">Low</option>
                                <option value="None">None</option>
                            </select>
                        </div>

                        <!-- Submit button -->
                        <button type="submit" class="btn btn-primary btn-block mb-4" >Save</button>
                        <input type="hidden" name="action" value="ajouterTache"></input>
                    </form>
                </div>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="view/js/home.js"></script>
    </body>
</html>