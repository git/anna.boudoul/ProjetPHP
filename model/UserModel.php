<?php
    require('business/User.php');
    require('dal/UserGateway.php');
    require_once("config/config.php");

    class UserModel{
        private $con; 
        private $gat;
        
        public function __construct(Connection $con) {
            $this->con = $con;
            $this->gat = new UserGateway($con);
        }
        
        function connexion($login, $mdp){
            Validation::val_form_texte($login, $TMessage);
            Validation::val_form_mdp($mdp, $TMessage);
            $result = $this->gat->findUser($login, $mdp);

            if(!isset($result)){
                return false;
            } else {
                $_SESSION['login'] = $result;
                return true;
            }  
        }

        function deconnexion(){
            session_unset();
            session_destroy();
        }

        function isConnected(){ //teste rôle dans la session, retourne instance d’objet ou booleen
            Validation::val_form_texte($_SESSION['login'], $TMessage);
            if(isset($_SESSION['login']) && $_SESSION['login']!="")
                return true;
            else
                return false;
        }

        function ajouter($login, $mdp){
            Validation::val_form_texte($login, $TMessage);
            Validation::val_form_mdp($mdp, $TMessage);
            $user = $this->findByLogin($login);
            if (empty($user))
                $this->gat->create($login, $mdp);
        }

        function supprimer($login){
            Validation::val_form_texte($login, $TMessage);
            $this->gat->delete($login);
        }

        function modifMdp($login, $mdp){
            Validation::val_form_texte($login, $TMessage);
            Validation::val_form_mdp($mdp, $TMessage);
            $this->gat->updateMdp($login, $mdp);
        }

        function findByLogin($login){
            Validation::val_form_texte($login, $TMessage);
            $user = null;
            if ($login !== " " && $login != null )
            {
                $results = $this->gat->find($login, 'login');
                foreach($results as $row){
                    $user = new User($results['login']);
                }
                return $user;
            }
        }

        function modifLogin($oldLogin, $newLogin){
            Validation::val_form_texte($oldLogin, $TMessage);
            Validation::val_form_texte($newLogin, $TMessage);
            $user = $this->findByLogin($oldLogin);
            if (empty($user))
                $this->gat->updateLogin($oldLogin, $newLogin);
        }
    }

?>