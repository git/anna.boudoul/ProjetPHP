Anna BOUDOUL

Nicolas FRANCO





#### Ce qui a été fait depuis l'oral:

- fonction `loadPrivateList` de la classe `TaskModel`
	* utilise deux nouvelles fonctions: `findUserList` et `findListTask` du gateway pour charger les listes privées d'un user

- fonction `loadPublicLists` de la classe `TaskModel`
	* utilise deux fonctions: `findPublicList` et `findListTask` du gateway pour charger les listes publiques 
    
- vue home.php
	* correction de la fonction `isConnected` qui est nécessaire dans cette vue
    
- contrôleur user
	* constructeur
	* méthode `deconnexion` associée à l'action `deconnecter`
    * méthode `newListPrivate` associée à l'action `creerListePriv`
    * méthode `loadListePriv` associée à l'action `voirListePriv`
	* méthode `erasePrivList` associée à l'action `supprimerListePriv` 

- vue connection.php
- vue newList.php
- vue register.php
- vue about.php
- vue newTask.php
- vue homePriv.php
- fin de la vue erreur.php

- navigation entre les pages avec `href=index.php?action=...`
- classe Validation
- utilisation de la classe validation dans le code
- "binding" des bouttons avec les actions correspondantes (pour les deux controlleurs, visitor et user)
- tableau de vues et tableau des messages d'erreurs
- corrections de bugs

#### Ce qui reste à faire

Nous avions envisagé de permettre à l'utilisateur d'afficher de plus amples informations sur les tâches, comme leurs dates de début et de fin et leur description, à l'aide d'un bouton sur nos vue home et home privé cependant nous n'avons pas eu le temps de le faire et par souci d'esthétisme et de lisibilité nous n'avons pas mis toutes ces informations dans le tableau des tâches pour chaque liste.

Nous souhaitions également afficher un compte des tâches faites sur le nombre de tâches total par liste mais nous n'avons pas eu le temps de terminer cette fonctionnalité.

Nous avons tout de même conservé les attributs qui étaient censés être utilisés dans ces fonctionnalités dans notre base de données et dans nos classes métiers en vue d'une possible amélioration de notre site.

#### Plus de détails
* vous pouvez trouver plus de détail sur notre dépot git disponible ici `https://codefirst.iut.uca.fr/git/anna.boudoul/ProjetPHP`, en regardant les commits qui ont été fait tout au long du projet.  

