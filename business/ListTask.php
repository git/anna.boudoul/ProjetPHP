<?php
    class ListTask{
        private int $id;
        private string $nom;
        private array $taches;
        private $owner;
        private int $dc; // done counter # juste visuelle??

        function __construct($nom, $owner="",$dc=0,$id=0) {
            if($id==0)
                $this->id = (int)null;
            else
                $this->id = $id;
            $this->nom = $nom;
            $this->taches = [];
            $this->owner = $owner;
            $this->dc = 0;
         }


        function get_id() {
            return $this->id;
        }

        function set_id($id) {
            $this->id = $id;
        }

        function get_nom() {
            return $this->nom;
        }

        function set_nom($nom) {
            $this->nom = $nom;
        }

        function get_taches() {
            return $this->taches;
        }

        function set_taches($taches) {
            $this->taches = $taches;
        }

        function get_owner() {
            if($this->owner == "")
                return NULL;

            return $this->owner;
        }

        function set_owner($owner) {
            $this->owner = $owner;
        }

        function get_dc() {
            return $this->dc;
        }

        function set_dc($dc) {
            $this->dc = $dc;
        }
    }
?>
