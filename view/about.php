<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="view/css/home.css" rel="stylesheet" />
    </head>
    <body>
        <div class="d-flex" id="wrapper">
            <!-- Sidebar-->
            <div class="border-end bg-white" id="sidebar-wrapper">
                <div class="sidebar-heading border-bottom bg-light">2do</div>
                <div class="list-group list-group-flush">
                <a class="list-group-item list-group-item-action list-group-item-light p-3" href="index.php">Home</a>
                <a class="list-group-item list-group-item-action list-group-item-light p-3" href="index.php?action=pageListe">New List +</a>
                <?php
                    if($user){
                        echo '<a class="list-group-item list-group-item-action list-group-item-light p-3" href="index.php?action=voirListePriv">My Lists &#128274;</a>';
                    }               
                ?>
                <a class="list-group-item list-group-item-action list-group-item-light p-3" href="index.php?action=pageAbout">About</a>
                </div>
            </div>
            <!-- Page content wrapper-->
            <div id="page-content-wrapper">
                <!-- Top navigation-->
                <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                    <div class="container-fluid">
                        <button class="btn btn-primary" id="sidebarToggle">Toggle Menu</button>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ms-auto mt-2 mt-lg-0">
                                <?php
                                    if($user){
                                        echo '<li class="nav-item"><a class="nav-link" href="index.php?action=deconnecter">Log out</a></li>';
                                    } else {
                                        echo '<li class="nav-item"><a class="nav-link" href="index.php?action=pageConnection">Log In</a></li>';
                                        echo '<li class="nav-item"><a class="nav-link" href="index.php?action=pageRegister">Register</a></li>';
                                    }
                                ?>
                            </ul>
                        </div>
                    </div>
                </nav>
                <!--Contenu de la page ici-->
                <div class="container-fluid">
                    <center>
                        <div class="col-lg-4 mb-5 mb-lg-0 text-center">
                            <div>
                            <div class="rounded-5 shadow-3-soft p-4" style="background-color: #fff9f2">
                                <div class="border-top border-dark mx-auto" style="width: 100px"></div>
                                <p class="text-muted mt-4 mb-2">2Do</p>
                                <p class="h5 mb-4" style="color: #344e41">A PHP project</p>
                                <p class="pb-4 mb-4">
                                    A little PHP project realised by two french students studying Computer Science at the IUT of
                                    Clermont Auvergne in France. The main goal of this project was to create a little To Do List
                                    using the PHP language. We hope that you will find it useful.
                                </p>
                                </div>
                            </div>
                        </div>
                    </center>
                </div>

            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="../view/js/home.js"></script>
    </body>
</html>