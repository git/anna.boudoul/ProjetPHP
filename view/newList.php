<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="view/css/home.css" rel="stylesheet" />
    </head>
    <body>
        <div class="d-flex" id="wrapper">
            <!-- Sidebar-->
            <div class="border-end bg-white" id="sidebar-wrapper">
                <div class="sidebar-heading border-bottom bg-light">2do</div>
                <div class="list-group list-group-flush">
                <a class="list-group-item list-group-item-action list-group-item-light p-3" href="index.php">Home</a>
                <a class="list-group-item list-group-item-action list-group-item-light p-3" href="index.php?action=pageListe">New List +</a>
                <?php
                    if($user){
                        echo '<a class="list-group-item list-group-item-action list-group-item-light p-3" href="index.php?action=voirListePriv">My Lists &#128274;</a>';
                    }               
                ?>
                <a class="list-group-item list-group-item-action list-group-item-light p-3" href="index.php?action=pageAbout">About</a>
                </div>
            </div>
            <!-- Page content wrapper-->
            <div id="page-content-wrapper">
                <!-- Top navigation-->
                <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                    <div class="container-fluid">
                        <button class="btn btn-primary" id="sidebarToggle">Toggle Menu</button>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ms-auto mt-2 mt-lg-0">
                                <?php
                                    if($user){
                                        echo '<li class="nav-item"><a class="nav-link" href="index.php?action=deconnecter">Log out</a></li>';
                                    } else {
                                        echo '<li class="nav-item"><a class="nav-link" href="index.php?action=pageConnection">Log In</a></li>';
                                        echo '<li class="nav-item"><a class="nav-link" href="index.php?action=pageRegister">Register</a></li>';
                                    }
                                ?>
                            </ul>
                        </div>
                    </div>
                </nav>
                <!--Contenue de la page ici-->
                <div class="container-fluid">
                    <section class="vh-50" style="background-color: #eee;">
                        <div class="container py-5 h-100">
                            <div class="row d-flex justify-content-center align-items-center h-100">
                                <div class="col col-lg-9 col-xl-7">
                                    <div class="card rounded-3"><div class="card rounded-3">
                                        <div class="card-body p-4">

                                            <h4 class="text-center my-3 pb-3">New List</h4>
                                            <form action="index.php" class="row row-cols-lg-auto g-3 justify-content-center align-items-center mb-4 pb-2" method="post">
                                                <div class="col-12">
                                                    <div class="form-outline">
                                                    <input type="text" id="form1" class="form-control" name="listName" required/>
                                                    </div>
                                                </div>

                                                <button type="submit" class="btn btn-primary">Add</button>
                                                <input type="hidden" name="action" value="creerListe"></input>
                                                
                                                <?php
                                                    if($user){
                                                        echo '
                                                        <label for="privateRadio" class="form-check-label">
                                                            <input class="form-check-input" type="radio" name="action" value="creerListePriv"> Private &#128274
                                                        </label>';
                                                    }
                                                ?>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="view/js/home.js"></script>
    </body>
</html>
