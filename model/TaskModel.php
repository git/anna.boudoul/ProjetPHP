<?php
include_once("dal/TaskGateway.php");
include_once("business/Task.php");

class TaskModel 
{
    public Connection $con;
    public TaskGateway $gtw;

    public function __construct(Connection $con)    
    {
        $this->con=$con;
        $this->gtw= new TaskGateway($con);
    }

    public function addTask($titre,$desc,$priorite,$idList,$dateDeb=null,$dateFin=null,$isDone=false,$id=0)
    {
        $t = new Task($titre,$desc,$priorite,$idList,$dateDeb,$dateFin,$isDone,$id);
        $this->gtw->insertT($t);
        // retourne quoi? con->lastInsertId() ?? 
    }

    public function supTask($id)
    {
       $this->gtw->delete('task',$id); 
    }

    public function modifTask($id,$element,$valeur)
    {
        $this->gtw->update('task',$id,$element,$valeur);
    }

    public function getAllTask()
    {
       return $this->gtw->find('task');
    }

    public function getTaskBy($element,$valeur)
    {
       return $this->gtw->find('task',$element,$valeur); 
    }
    
    /* LIST FUNCTIONS */
    public function addList($nom,$owner="",$dc=0,$id=0)
    {
        Validation::val_form_texte($owner, $TMessage);
        $l = new ListTask($nom,$owner,$dc);
        $this->gtw->insertL($l);
        // retourne quoi? con->lastInsertId() ?? 
    }
    
    public function modifList($id,$element,$valeur)
    {
       $this->gtw->update('list',$id,$element,$valeur);
    }

    public function size($list){
        $taille = $this->gtw->count($list);
        return $taille;
    }
 
    public function supList($id)
    {
        $this->gtw->delete('list',$id);
    }

    function loadPublicLists()
    {
        $lists = $this->gtw->findPublicList(); 

        foreach($lists as &$row){
            $row = $this->gtw->findListTask($row);
        }
        return $lists;
    }

    function loadPrivateLists($user){
        # prend toutes les listes de l'user
        $lists = $this->gtw->findUserList($user);

        # pour chacune de ses listes, charge les taches
        foreach($lists as &$row){
            $row = $this->gtw->findListTask($row);
        }
        return $lists;
    }
}
?>
