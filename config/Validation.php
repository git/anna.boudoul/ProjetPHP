<?php

    class Validation {

        static function val_form_texte(&$texte, &$TMessage) {
            if (!isset($texte)||$texte=="") {
                $TMessage[] ="Empty fields";
                $texte="";
            }

            if ($texte != filter_var($texte, FILTER_SANITIZE_STRING))
            {
                $TMessage[]="Attempt to inject code (security attack)";
                $texte="";
            }

        }

        static function val_form_mdp(&$mdp, &$TMessage) {
            if (!isset($mdp)||$mdp=="") {
                $TMessage[] ="Password not specified";
                $mdp="";
            }

            if ($mdp != filter_var($mdp, FILTER_SANITIZE_SPECIAL_CHARS))
            {
                $TMessage[] ="Password must not contain special characters";
                $mdp="";
            }

        }
    }
?>