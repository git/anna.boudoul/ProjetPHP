<?php
require_once("config/config.php");
require("model/UserModel.php");

class FrontCtrl
{
    private UserModel $usrMdl;
    private $action_User;
    private bool $isUser;
    private $TabVues;

    function __construct(&$con, $TabVues){
        session_start();
        $this->TabVues = $TabVues;
        $this->usrMdl = new UserModel($con);
        $this->action_User = array('deconnecter', 'voirListePriv', 'creerListePriv', 'supprimerListePriv');
        try{
            
            $this->isUser = $this->usrMdl->isConnected(); 
            $action = $_REQUEST['action'] ?? null;
    
            if(($i = array_search($action,$this->action_User)) !== false){ # si action dans la liste d'actions user
                if(!$this->isUser){ # si pas conncter
                    # appel controlleur visiteur avec action connecter
                    require("VisitorCtrl.php");
                    $visitCtrl = new VisitorCtrl($con, $this->TabVues, $this->isUser);
                    $visitCtrl->goconnexion();
                } else { # sinon
                    # handle action avec controlleur user
                    require("UserCtrl.php"); 
                    $userCtrl = new UserCtrl($con, $this->TabVues);
                }
                
            } else { # sinon forcement action visiteur
                # appel controlleur visiteur avec l'action
                require("VisitorCtrl.php"); 
                $visitCtrl = new VisitorCtrl($con, $this->TabVues, $this->isUser);
            }
        } catch (Exception $e){ // verifier si catch bon
            $TMessage[] = $e->getMessage();
            require($this->TabVues["erreur"]); 
        }
    }
}
?>
