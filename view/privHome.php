<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="view/css/home.css" rel="stylesheet" />
    </head>
    <body>
        <div class="d-flex" id="wrapper">
            <!-- Sidebar-->
            <div class="border-end bg-white" id="sidebar-wrapper">
                <div class="sidebar-heading border-bottom bg-light">2do</div>
                <div class="list-group list-group-flush">
                <a class="list-group-item list-group-item-action list-group-item-light p-3" href="index.php">Home</a>
                <a class="list-group-item list-group-item-action list-group-item-light p-3" href="index.php?action=pageListe">New List +</a>
                <a class="list-group-item list-group-item-action list-group-item-light p-3" href="index.php?action=voirListePriv">My Lists &#128274;</a>
                <a class="list-group-item list-group-item-action list-group-item-light p-3" href="index.php?action=pageAbout">About</a>
                </div>
            </div>
            <!-- Page content wrapper-->
            <div id="page-content-wrapper">
                <!-- Top navigation-->
                <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                    <div class="container-fluid">
                        <button class="btn btn-primary" id="sidebarToggle">Toggle Menu</button>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ms-auto mt-2 mt-lg-0">
                                <li class="nav-item"><a class="nav-link" href="index.php?action=deconnecter">Log out</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <!-- Page content-->
                <div class="container-fluid">
                    <h1 class="mt-4">My Lists</h1>
                    
                    <?php
                        if(empty($private_lists)){
                            echo'
                            <div class="d-flex align-items-center justify-content-center">
                                <a href="index.php?action=pageListe" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-envelope"></span>Add List &#10133;</a>
                            </div>';
                        }
                        foreach($private_lists as $l){
                            echo '
                            <section class="vh-0" style="background-color: #eee;">
            <div class="container py-5 h-100">
                <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col col-lg-9 col-xl-7">
                        <div class="card rounded-3"><div class="card rounded-3">
                            <div class="card-body p-4">
                                <form action="index.php" method="post" class="row row-cols-lg-auto g-3 justify-content-center align-items-center mb-4 pb-2">         
                                    <div class="col-12 position-absolute top-0 start-0">
                                        <button type="submit" class="btn btn-danger">&#128465;</button>
                                        <input type="hidden" name="listId" value="'.$l->get_id().'"></input>
                                        <input type="hidden" name="action" value="supprimerListePriv"></input>
                                    </div>
                                </form>
                                <h4 class="text-center my-3 pb-3">'.$l->get_nom().'</h4>
                                <form action="index.php" method="post" class="row row-cols-lg-auto g-3 justify-content-center align-items-center mb-4 pb-2">
                                              
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-primary">New task &#128221;</button>
                                        <input type="hidden" name="listId" value="'.$l->get_id().'"></input>
                                        <input type="hidden" name="action" value="pageTache"></input>
                                        <input type="hidden" name="isPriv" value="true"></input>
                                    </div>
                                </form>
                                <table class="table mb-4">
                                <thead>
                                    <tr>
                                        <th scope="col">Task</th>
                                        <th scope="col">Importance</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>';
                                # IF NO TASKS, display NO TASKS
                            if(!empty($l->get_taches())){
                                foreach($l->get_taches() as $t){
                                    if(!$t->get_isDone()){
                                        echo '
                                        <tr>
                                            <td>'.$t->get_titre().'</td>
                                            <td>'.$t->get_priorite().'</td>';
                                    } else {
                                        echo '
                                        <tr>
                                            <td><del>'.$t->get_titre().'</del></td>
                                            <td><del>'.$t->get_priorite().'</del></td>';
                                    }
                                    echo '
                                    <td>
                                    <form action="index.php" method="post" class="d-inline-block">';
                                    if(!$t->get_isDone()){
                                        echo '<button type="submit" class="btn btn-success ms-1">&#10004;
                                            <input type="hidden" name="idT" value="'.$t->get_id().'"></input>
                                            <input type="hidden" name="Tdone" value="'.$t->get_isDone().'"></input>
                                            <input type="hidden" name="action" value="isDone">
                                            <input type="hidden" name="isPriv" value="true">
                                            </button>';
                                    } else {
                                        echo '<button type="submit" class="btn btn-secondary ms-1">&#10006;
                                        <input type="hidden" name="idT" value="'.$t->get_id().'"></input>
                                        <input type="hidden" name="Tdone" value="'.$t->get_isDone().'"></input>
                                        <input type="hidden" name="action" value="isDone">
                                        <input type="hidden" name="isPriv" value="true">
                                        </button>';
                                    }
                                    echo '
                                    </form>
                                    <form action="index.php" method="post" class="d-inline-block">
                                      <button type="submit" class="btn btn-danger">&#128465;
                                      <input type="hidden" name="idT" value="'.$t->get_id().'"></input>
                                      <input type="hidden" name="action" value="supprimerTache">
                                      <input type="hidden" name="isPriv" value="true">
                                      </button>
                                    </form>
                                    </td>
                                  </tr>';
                                } 
                            } else {
                               echo '<h6 class="text-center my-3 pb-3">No tasks here yet!</h6>';
                            }
                                 
                            echo ' </tbody>
                                 </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>';
                        }
                    ?>

                </div>
            </div>
        </div>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="view/js/home.js"></script>
    </body>
</html>

